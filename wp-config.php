<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pluteos' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lU4j}8{]MHmr.Q@5G4tf7Z&muRt=vbqefzRheTR1/73Ws{@+#~|=n!:|=j&*b3nV' );
define( 'SECURE_AUTH_KEY',  'MAPd(GF;2)tx^S0X^3RA9ZSKr?$|:#`x%K$M3DSnu#4t*zI_;Jb:;^=+sgvYd]8A' );
define( 'LOGGED_IN_KEY',    '8*Gq]_49*jW7Z>k5,hOeMTt[Pr@rSl~=$Pf!f^Pv( ]qz79Z%<(1-7=w9fmymGYJ' );
define( 'NONCE_KEY',        'Qnk@VuCsNQ 8J=o6$:eVeoqtbl8_&tNa2$jk{SZ5d]-eUtm2:bjo-l^fciS;B.u@' );
define( 'AUTH_SALT',        'lu~1 [3:<#LuJSM>xj9y$H^.>Q2uJzT(4wVfn %c>/e.<j-!2(AHW7B}Is5/hIn:' );
define( 'SECURE_AUTH_SALT', 'G>DXOc>c)/&G7Yd@}o|CRqc>|93/++8q#TATiC:Mr2y1fb|86.i+L1[4p@+ T]#F' );
define( 'LOGGED_IN_SALT',   'M?#=Bg]C6[1[zEvC~{YZ70-TDwwyi]DrgGc<3&M4#E[sfwD=X@U8){9TVLm-jlyi' );
define( 'NONCE_SALT',       'r>F4?BV)oJ9?+g?GR4o@:KimQ}L8@u/nPbz}R_tB{?9]V78On5jE4opFen*s+uGo' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
