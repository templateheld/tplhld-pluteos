// Classes
function NavToggle() {
  this.elems = {
    nav: '#main-nav-wrapper',
    trigger: {
      elem: '.main-nav-toggle',
      toggle: 'data-toggle'
    }
  },
  this.states = {
    active: 'active',
    bodyState: 'nav-active'
  },
  this.toggle = function() {
    var navToggle = this,
        toggle;

    jQuery(document).on('click', function(event) {
      trigger = jQuery(event.target).closest(navToggle.elems.trigger.elem);
      wrapper = jQuery(event.target).closest(navToggle.elems.nav);
      toggle = jQuery(trigger).attr(navToggle.elems.trigger.toggle);

      if (trigger.length && toggle == 'open') {
        jQuery(navToggle.elems.nav).addClass(navToggle.states.active);
        jQuery('body').addClass(navToggle.states.bodyState);
      }

      if (!trigger.length && !wrapper.length || toggle == 'close'){
        jQuery(navToggle.elems.nav).removeClass(navToggle.states.active);
        jQuery('body').removeClass(navToggle.states.bodyState);
      }
    });
  },
  this.init = function() {
    this.toggle();
  }
}

function LazyLoad() {
  this.init = function() {
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.expand = 10; //default 360-500
    lazySizesConfig.expFactor = 1.5; //default: 1.7
  }
}

function ScrollSpy() {
  this.elems = {
    selector: '[data-scrollspy]',
    body: 'body'
  },
  this.states = {
    isInView: 'in-view'
  },
  this.checkIsMobile = function() {
    return window.matchMedia('(max-width: 991px)');
  }
  this.init = function() {
    // Used in conjunction with jquey.appear (https://github.com/morr/jquery.appear)
    var scrollspy = this
        isMobile = this.checkIsMobile();


    // Workaround for .force_appear() function
    // .force_appear() is used for mobile views if elements are already in view
    // if (isMobile.matches) {
      setTimeout(function() {
        $.force_appear();
      },1);
    // }

    // Init appear js for all scrollspy elems
    jQuery(this.elems.selector).appear();

    jQuery(this.elems.body).on('appear', this.elems.selector, function(event, $affected) {

      // If elem is in view
      $affected.each(function(index) {
        jQuery(this).addClass(scrollspy.states.isInView);
      });
    });
  }
}

function HomeIntro() {
  this.elems = {
    intro: '.__home-intro-animation',
    body: '[data-animation-body]'
  },
  this.steps = {
    durations: {
      step1: 0,
      step2: 325 + 650,
      step3: 325 + 650 + 2600 + 1950,
      step4: 325 + 650 + 2600 + 1950,
      step5: 325 + 650 + 2600 + 1950 + 1300,
      step6: 325 + 650 + 2600 + 1950 + 1300 + 1950,
      stepfinal: 325 + 650 + 2600 + 1950 + 1300 + 1950 + 3900
    }
  },
  this.init = function() {
    var homeIntro = this;

    if (jQuery(this.elems.intro).length) {
      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-2', '');
      },homeIntro.steps.durations.step1);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-3', '');
      },homeIntro.steps.durations.step2);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-4', '');
      },homeIntro.steps.durations.step3);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-5', '');
      },homeIntro.steps.durations.step4);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-6', '');
      },homeIntro.steps.durations.step5);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).attr('data-step-7', '');
      },homeIntro.steps.durations.step6);

      setTimeout(function() {
        jQuery(homeIntro.elems.body).removeAttr('data-step-1');
        jQuery(homeIntro.elems.body).removeAttr('data-step-2');
        jQuery(homeIntro.elems.body).removeAttr('data-step-3');
        jQuery(homeIntro.elems.body).removeAttr('data-step-4');
        jQuery(homeIntro.elems.body).removeAttr('data-step-5');
        jQuery(homeIntro.elems.body).removeAttr('data-step-6');
        jQuery(homeIntro.elems.body).removeAttr('data-step-7');
        jQuery(homeIntro.elems.body).removeAttr('data-animation-body');
      },homeIntro.steps.durations.stepfinal);
    }
  }
}

function AccordionPlus() {
  this.elems = {
    collapsibles: '#accordion .content-block-wrapper',
    wrapper: '.content-block-image-wrapper',
    header: '#header'
  },
  this.setHeight = function(elem) {
    var height = jQuery(elem).height();

    jQuery(elem).css({
      'height': height
    });
  },
  this.init = function() {
    var accordionPlus = this,
        wrapper,
        headerHeight = jQuery(this.elems.header).outerHeight();

    jQuery(this.elems.collapsibles).on('shown.bs.collapse', function() {
      wrapper = jQuery(this).find(accordionPlus.elems.wrapper);

      jQuery('html, body').animate({
        scrollTop:jQuery(this).offset().top - headerHeight
      },'slow');

      accordionPlus.setHeight(wrapper);
    });
  }
}

// Objects
var navToggle = new NavToggle();
var lazyLoad = new LazyLoad();
var scrollSpy = new ScrollSpy();
var homeIntro = new HomeIntro();
var accordionPlus = new AccordionPlus();

lazyLoad.init();

// DOM
jQuery(document).ready(function() {
  navToggle.init();
  scrollSpy.init();
  homeIntro.init();
  accordionPlus.init();
});
