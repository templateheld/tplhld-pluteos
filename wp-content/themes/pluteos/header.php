<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */
 $body_attrs = false;
 $is_multi_lingual = false;

 if (is_front_page()) {
   $body_attrs = 'data-animation-body data-step-1';
 }

 $frontpage_id = get_option( 'page_on_front' );
 $frontpage_german_id = pll_get_post($frontpage_id, 'de');
 $frontpage_german = get_post($frontpage_german_id);

 if ($frontpage_german->post_status == 'publish') {
   $is_multi_lingual = true;
 }

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/safari-pinned-tab.svg" color="#33303f">
    <meta name="msapplication-TileColor" content="#33303f">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Pluteos AG is a private intelligence agency.">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php echo $body_attrs; ?>>
	<?php if (!is_front_page()) { ?>
		<div id="bg-image"></div>
	<?php } ?>
	<header id="header">
    <div class="row">
      <div class="logo">
        <a href="<?php echo get_home_url(); ?>">
          <img src="<?php echo the_field('logo_header', $frontpage_id); ?>" alt="">
        </a>
      </div>
      <div id="slogan">
        <h2><?php echo the_field('claim', $frontpage_id) ?></h2>
      </div>
      <nav id="main-nav">
        <button class="main-nav-toggle" type="button" aria-label="Open" data-toggle="open"></button>
        <div id="main-nav-wrapper">
          <div class="main-nav-head">
            <?php if (function_exists('pll_the_languages') && $is_multi_lingual): ?>
              <div id="language-switch">
                <ul><?php pll_the_languages();?></ul>
              </div>
            <?php endif; ?>
            <div class="main-nav-toggle-container">
              <button class="main-nav-toggle" type="button" aria-label="Close" data-toggle="close"></button>
            </div>
          </div>
          <div class="main-nav-body">
            <div class="main-nav-body-wrapper">
              <?php
                $menu = array(
                  'container'      	=> '',
                  'theme_location'	=> 'menu-1',
                  'menu_id'       	=> 'primary-menu',
                  'link_before'			=> '<span>',
                  'link_after'			=> '</span>'
                );

                wp_nav_menu($menu);
              ?>
            </div>
          </div>
        </div>
      </nav>
    </div>
	</header>
