<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 * Template Name: Sector & Clients
 */

get_header();
?>
<main id="content">

		<?php while ( have_posts() ) { the_post();

			get_template_part( 'template-parts/content', 'sector-clients' );

		} ?>

</main>

<?php get_footer(); ?>
