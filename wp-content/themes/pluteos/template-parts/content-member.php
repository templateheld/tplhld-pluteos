<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<header class="content-block __overlay-bg">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-8 mr-auto ml-auto content-block-text">
          <h1><?php echo the_field('titel'); ?><br><small><?php echo the_field('position'); ?></small></h1>
        </div>
      </div>
    </div>
  </div>
</header>

<address class="content-block __member-detail">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-md-6 ml-auto mr-auto content-block-image">
          <?php
            $image = get_field('bild',pll_get_post(get_the_ID(),'en'));
            $src = wp_get_attachment_image_src( $image, 'full' )[0];
            $src_small = wp_get_attachment_image_src( $image, 'medium' )[0];
            $srcset = wp_get_attachment_image_srcset( $image, 'full' );
            $sizes = wp_get_attachment_image_sizes( $image, 'full' );
            $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

            if( $image ) {
              ?>
              <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
              data-srcset="<?php echo esc_attr( $srcset ); ?>"
              data-sizes="<?php echo esc_attr( $sizes );?>"
              alt="<?php echo esc_attr( $alt );?>"
              class="lazyload">
          <?php } ?>
        </div>
        <div class="col-10 col-md-6 ml-auto mr-auto content-block-text">
          <div class="content-block-text-wrapper">

            <?php if (!empty(get_field('telefon'))): ?>
              <p>
                <small><?php echo pll__('PHONE'); ?></small><br>
                <a href="tel:<?php echo the_field('telefon'); ?>"><?php echo the_field('telefon'); ?></a>
              </p>
            <?php endif; ?>

            <?php if (!empty(get_field('e-mail'))): ?>
              <p>
                <small><?php echo pll__('EMAIL'); ?></small><br>
                <a href="tel:<?php echo the_field('e-mail'); ?>"><?php echo the_field('e-mail'); ?></a>
              </p>
            <?php endif; ?>

            <?php if (!empty(get_field('adresse'))): ?>
              <p>
                <small><?php echo pll__('ADDRESS'); ?></small><br>
                <?php echo the_field('adresse'); ?>
              </p>
            <?php endif; ?>

            <?php if (!empty(get_field('vcf'))): ?>
              <p><a href="<?php echo the_field('vcf'); ?>" class="btn-primary btn-small"><?php echo pll__('VCF Card'); ?></a></p>
            <?php endif; ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</address>

<div class="content-block __two-column-text">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-lg-5 ml-auto mr-auto content-block-text"  data-scrollspy data-appear-top-offset="-50">
            <h2 class="h3"><?php echo the_field('competence_titel'); ?></h2>
            <?php echo the_field('competence_content'); ?>
        </div>
        <div class="col-10 col-lg-5 ml-auto mr-auto content-block-text"  data-scrollspy data-appear-top-offset="-50">
            <h2 class="h3"><?php echo pll__('VITA'); ?></h2>
            <?php echo the_field('vita'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<footer class="content-block __overlay-bg">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm ml-auto mr-auto content-block-text">
          <p><a href="<?php echo the_field('link'); ?>" class="btn-primary"><?php echo the_field('link_titel'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
