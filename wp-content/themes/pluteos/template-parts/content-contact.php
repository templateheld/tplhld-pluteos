<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<header class="content-block __overlay-bg">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <h1><?php echo the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</header>

<address class="content-block __one-column-text text-center">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <?php if (!empty(get_field('telefon'))): ?>
            <p>
              <small><?php echo pll__('PHONE'); ?></small><br>
              <a href="tel:<?php echo the_field('telefon'); ?>"><?php echo the_field('telefon'); ?></a>
            </p>
          <?php endif; ?>

          <?php if (!empty(get_field('e-mail'))): ?>
            <p>
              <small><?php echo pll__('EMAIL'); ?></small><br>
              <a href="tel:<?php echo the_field('e-mail'); ?>"><?php echo the_field('e-mail'); ?></a>
            </p>
          <?php endif; ?>

          <?php if (!empty(get_field('adresse'))): ?>
            <p>
              <small><?php echo pll__('ADDRESS'); ?></small><br>
              <?php echo the_field('adresse'); ?>
            </p>
          <?php endif; ?>

          <?php if (!empty(get_field('anfahrt_link'))): ?>
            <p><a href="<?php echo the_field('anfahrt_link'); ?>" target="_blank" class="btn-primary"><?php echo the_field('anfahrt_title'); ?></a></p>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</address>

<div class="content-block __google-maps" data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row __equal-row">
        <div class="col-10 col-md-6 ml-auto mr-auto content-block-text">
          <?php
            $image = get_field('bild','14');
            $src = wp_get_attachment_image_src( $image, 'full' )[0];
            $srcset = wp_get_attachment_image_srcset( $image, 'full' );
            $sizes = wp_get_attachment_image_sizes( $image, 'full' );
            $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

            if( $image ) {
              ?>
              <img src="<?php echo esc_attr( $src );?>"
              srcset="<?php echo esc_attr( $srcset ); ?>"
              sizes="<?php echo esc_attr( $sizes );?>"
              alt="<?php echo the_field('titel'); ?>">
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content-block __one-column-text text-center" data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-9 ml-auto mr-auto content-block-text">
            <?php if( have_rows('footer_links') ): ?>
            <?php
                $fields = get_field_object('footer_links');
                $count = (count($fields['value']));
                $i = 1;
                while( have_rows('footer_links') ): the_row();
            ?>
                <?php #var_dump($i) ?>
                <p><a href="<?php the_sub_field('link'); ?>" class="btn-primary"><?php the_sub_field('link_titel'); ?></a></p>
                <?php if( $i != $count ): ?>
                    <p>&nbsp;</p>
                <?php endif; ?>
            <?php
                $i++;
                endwhile;
            ?>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
