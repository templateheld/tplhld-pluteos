<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<header class="content-block __overlay-bg">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <h1><?php echo pll__( 'Page not found'); ?></h1>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="content-block __one-column-text text-center">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <p><?php echo pll__( 'Sorry, but we couldn\'t find the page you are looking for.'); ?></p>
					<p>
						<a class="btn-primary" href="<?php echo get_home_url(); ?>">
							<?php echo pll__( 'Return to homepage'); ?>
						</a>
					</p>
        </div>
      </div>
    </div>
  </div>
</div>
