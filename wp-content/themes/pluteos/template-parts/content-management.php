<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

//preparing standard pic from default en version in array
$images = array();
$k = 0;
while( have_rows('members', pll_get_post(get_the_ID(),'en')) ): the_row();

    $image = get_sub_field('bild');
    $images[$k]['image'] = $image;
    $images[$k]['src'] = wp_get_attachment_image_src( $image, 'full' )[0];
    $images[$k]['src_small'] = wp_get_attachment_image_src( $image, 'medium' )[0];
    $images[$k]['srcset'] = wp_get_attachment_image_srcset( $image, 'full' );
    $images[$k]['sizes'] = wp_get_attachment_image_sizes( $image, 'full' );
$k++;
endwhile;

?>
<header class="content-block __gradient">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-md-8 mr-auto ml-auto content-block-text">
          <h1><?php echo the_field('board_titel'); ?></h1>
          <?php echo the_field('board_subtitel'); ?>
        </div>
      </div>
    </div>
  </div>
</header>

<?php if( have_rows('members') ): ?>
    <?php
      $i = 0;
      while( have_rows('members') ): the_row();


        #if ($i > 1) {
          $block_attr = ' data-scrollspy data-appear-top-offset="-50"';
        #}
     ?>
        <article class="content-block __team-member"<?php echo $block_attr; ?>>
            <div class="content-block-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-10 col-md-6 ml-auto mr-auto content-block-image">
                            <a href="<?php the_sub_field('link'); ?>">
                              <?php

                                if( count($images) ) {
                                  ?>
                                  <img src="<?php echo esc_attr( $images[$i]['src_small'] );?>" data-src="<?php echo esc_attr( $images[$i]['src'] );?>"
                                  data-srcset="<?php echo esc_attr( $images[$i]['srcset'] ); ?>"
                                  data-sizes="<?php echo esc_attr( $images[$i]['sizes'] );?>"
                                  alt="<?php the_sub_field('name'); ?> <?php the_sub_field('position'); ?>"
                                  class="lazyload">
                              <?php } else { ?>

                                  <img src="<?php echo esc_attr( $images[$i]['src'] );?>"
                                  srcset="<?php echo esc_attr(  $images[$i]['srcset'] ); ?>"
                                  sizes="<?php echo esc_attr( $images[$i]['sizes'] );?>"
                                  alt="<?php the_sub_field('name'); ?> <?php the_sub_field('position'); ?>">

                              <?php } ?>
                                <div class="content-block-image-caption">
                                    <h3 class="p"><?php the_sub_field('vita_titel'); ?></h3>
                                    <?php the_sub_field('vita'); ?>
                                </div>
                            </a>
                        </div>
                        <div class="col-10 col-md-6 ml-auto mr-auto content-block-text">
                            <div class="content-block-text-wrapper">
                                <h2 class="p"><?php the_sub_field('name'); ?><br><small><?php the_sub_field('position'); ?></small></h2>
                                <p><a class="btn-primary" href="<?php the_sub_field('link'); ?>"><?php the_sub_field('link_titel'); ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    <?php

        $i++;
        endwhile;
    endif;

    ?>

<article class="content-block __one-column-text __margin-top-small" data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-0 col-sm-1"></div>
        <div class="col-10 ml-auto mr-auto mr-sm-none content-block-text">
          <h2 class="h1"><?php echo the_field('footer_titel'); ?></h2>
          <?php echo the_field('footer_text'); ?>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p class="text-right"><a class="btn-primary" href="<?php echo the_field('button_link'); ?>"><?php echo the_field('button_name'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</article>
