<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

 $frontpage_id = get_option( 'page_on_front' );

?>

<header class="content-block __intro">
  <div class="content-image center">
    <?php
      $image = get_field('bild', pll_get_post(get_the_ID(),'en'));
      $src = wp_get_attachment_image_src( $image, 'full' )[0];
      $src_small = wp_get_attachment_image_src( $image, 'large' )[0];
      $srcset = wp_get_attachment_image_srcset( $image, 'full' );
      $sizes = wp_get_attachment_image_sizes( $image, 'full' );
      $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

      if( $image ) {
        ?>
        <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
        data-srcset="<?php echo esc_attr( $srcset ); ?>"
        data-sizes="<?php echo esc_attr( $sizes );?>"
        alt="<?php echo esc_attr( $alt );?>"
        class="lazyload">
    <?php } ?>
  </div>
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12 col-xl-2"></div>
        <div class="col-10 col-xl-8 content-block-text">
          <h1><?php echo the_field('titel'); ?></h1>
        </div>
        <div class="col-2 content-block-image">
          <img src="<?php echo the_field('logo_p', $frontpage_id); ?>" alt="">
        </div>
      </div>
    </div>
  </div>
</header>

<div class="content-block __one-column-text-small __overlay-bg"  data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10 col-sm-9 ml-auto mr-auto content-block-text">
          <h2 class="h1"><?php echo the_field('blackbox_titel'); ?></h2>
          <?php echo the_field('blackbox_content'); ?>
        </div>
      </div>
    </div>
  </div>
</div>



<?php if( have_rows('content_blocke') ): ?>
  <?php
      $i = 1;
      while( have_rows('content_blocke') ): the_row();
  ?>

  <div class="content-block __one-column-text-small"  data-scrollspy data-appear-top-offset="-50">
    <div class="content-block-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-1"></div>
          <div class="col-10 col-sm-9 ml-auto mr-auto content-block-text">
            <p><?php echo the_sub_field('title'); ?><br><?php echo the_sub_field('content'); ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
    $i++;
    endwhile;
  ?>
<?php endif; ?>

<div class="content-block __one-column-text-small __margin-bottom-big"  data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-1"></div>
        <div class="col-10 col-sm-9 ml-auto mr-auto content-block-text">
          <p>&nbsp;</p>
          <p class="text-right"><a href="<?php echo the_field('button_link'); ?>" class="btn-primary btn-big"><?php echo the_field('button_text'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</div>
