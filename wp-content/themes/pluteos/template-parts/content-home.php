<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

 $frontpage_id = get_option( 'page_on_front' );
?>

<header class="content-block __home-intro __home-intro-animation">
  <div class="content-image">
    <?php
      $image = get_field('sektion_1_hintergrund', pll_get_post(get_the_ID(),'en'));
      $src = wp_get_attachment_image_src( $image, 'full' )[0];
      $src_small = wp_get_attachment_image_src( $image, 'large' )[0];
      $srcset = wp_get_attachment_image_srcset( $image, 'full' );
      $sizes = wp_get_attachment_image_sizes( $image, 'full' );
      $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

      if( $image ) {
        ?>
        <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
        data-srcset="<?php echo esc_attr( $srcset ); ?>"
        data-sizes="<?php echo esc_attr( $sizes );?>"
        alt="<?php echo esc_attr( $alt );?>"
        class="lazyload">
    <?php } ?>
  </div>
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12 col-xl-2"></div>
        <div class="col-10 col-xl-8 content-block-text">
          <h1><?php echo the_field('sektion_1_titel'); ?></h1>
        </div>
        <div class="col-2 content-block-image">
          <img src="<?php echo the_field('logo_p', $frontpage_id); ?>" alt="">
        </div>
      </div>
    </div>
  </div>
</header>

<article class="content-block __divider" data-scrollspy data-appear-top-offset="50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 ml-auto mr-auto content-block-text">
          <h2><?php echo the_field('sektion_1_subtitel'); ?></h2>
        </div>
      </div>
    </div>
  </div>
</article>

<article class="content-block __bottom-content">
  <div class="content-image right">
    <?php
      $image = get_field('sektion_2_hintergrund', pll_get_post(get_the_ID(),'en'));
      $src = wp_get_attachment_image_src( $image, 'full' )[0];
      $src_small = wp_get_attachment_image_src( $image, 'large' )[0];
      $srcset = wp_get_attachment_image_srcset( $image, 'full' );
      $sizes = wp_get_attachment_image_sizes( $image, 'full' );
      $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

      if( $image ) {
        ?>
        <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
        data-srcset="<?php echo esc_attr( $srcset ); ?>"
        data-sizes="<?php echo esc_attr( $sizes );?>"
        alt="<?php echo esc_attr( $alt );?>"
        class="lazyload">
    <?php } ?>
  </div>
</article>

<article class="content-block __divider text-left" data-scrollspy data-appear-top-offset="-50">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 ml-auto mr-auto content-block-text">
          <p><?php echo the_field('sektion_3_text'); ?></p>
        </div>
      </div>
    </div>
  </div>
</article>

<article class="content-block __sideway-content"  data-scrollspy data-appear-top-offset="-150">
  <div class="content-image right">
    <?php
      $image = get_field('sektion_3_hintergrund', pll_get_post(get_the_ID(),'en'));
      $src = wp_get_attachment_image_src( $image, 'full' )[0];
      $src_small = wp_get_attachment_image_src( $image, 'large' )[0];
      $srcset = wp_get_attachment_image_srcset( $image, 'full' );
      $sizes = wp_get_attachment_image_sizes( $image, 'full' );
      $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

      if( $image ) {
        ?>
        <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
        data-srcset="<?php echo esc_attr( $srcset ); ?>"
        data-sizes="<?php echo esc_attr( $sizes );?>"
        alt="<?php echo esc_attr( $alt );?>"
        class="lazyload">
    <?php } ?>
  </div>
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-6 col-lg-5 col-xl-4 ml-auto content-block-text">
          <p><a class="btn-primary btn-big" href="<?php echo the_field('sektion_3_link'); ?>"><?php echo the_field('sektion_3_link_titel'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</article>
