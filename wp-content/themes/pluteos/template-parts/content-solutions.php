<?php
/**
 * Template part for displaying page content in page-home.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */
?>

<header class="content-block __overlay-absolute __bg-dark-transparent __margin-top-big text-left">
  <div class="content-image center">
    <?php
      $image = get_field('bild', pll_get_post(get_the_ID(),'en'));
      $src = wp_get_attachment_image_src( $image, 'full' )[0];
      $src_small = wp_get_attachment_image_src( $image, 'large' )[0];
      $srcset = wp_get_attachment_image_srcset( $image, 'full' );
      $sizes = wp_get_attachment_image_sizes( $image, 'full' );
      $alt = get_post_meta( $image, '_wp_attachment_image_alt', true);

      if( $image ) {
        ?>
        <img src="<?php echo esc_attr( $src_small );?>" data-src="<?php echo esc_attr( $src );?>"
        data-srcset="<?php echo esc_attr( $srcset ); ?>"
        data-sizes="<?php echo esc_attr( $sizes );?>"
        alt="<?php echo esc_attr( $alt );?>"
        class="lazyload">
    <?php } ?>
  </div>
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm ml-auto mr-auto content-block-text">
          <h1><?php echo the_field('titel'); ?></h1>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="content-block __overlay-bg text-left">
  <div class="content-block-wrapper" data-scrollspy data-appear-top-offset="-50">
    <div class="container">
      <div class="row">
        <div class="col-0 col-sm-1"></div>
        <div class="col-10 col-sm-9 ml-auto mr-auto content-block-text">
            <?php echo the_field('content'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="matrix" class="content-block __matrix-block" data-scrollspy data-appear-top-offset="-50">

<?php if( have_rows('facts') ): ?>

  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">

        <?php
            $i = 1;
            while( have_rows('facts') ): the_row();
        ?>

        <a href="<?php the_sub_field('link'); ?>" class="col-12 col-sm-6 col-md-4 col-lg-3 content-block-text">
          <div class="content-block-image-wrapper">
            <?php
              $image = get_sub_field('bild', pll_get_post(get_the_ID(),'en'));
              $src = wp_get_attachment_image_src( $image, 'medium' )[0];
              $srcset = wp_get_attachment_image_srcset( $image, 'medium' );
              $sizes = wp_get_attachment_image_sizes( $image, 'medium' );

              if( $image ) {
                ?>
                <img src="<?php echo esc_attr( $src );?>"
                srcset="<?php echo esc_attr( $srcset ); ?>"
                sizes="<?php echo esc_attr( $sizes );?>"
                alt="<?php the_sub_field('titel'); ?>"
                >
            <?php } ?>
          </div>
          <p><?php the_sub_field('titel'); ?></p>
        </a>

        <?php
            $i++;
            endwhile;
        ?>

      </div>
    </div>
  </div>

<?php endif; ?>

</div>

<footer class="content-block __overlay-bg-2">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm ml-auto mr-auto content-block-text">
          <p class="text-right"><a href="<?php echo the_field('button_link'); ?>" class="btn-primary"><?php echo the_field('button_titel'); ?></a></p>
        </div>
      </div>
    </div>
  </div>
</footer>
