<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Templateheld
 */

?>

<header class="content-block __overlay-bg">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <h1><?php echo the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="content-block __one-column-text text-center">
  <div class="content-block-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-10 col-sm-8 mr-auto ml-auto content-block-text">
          <?php echo the_content(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
