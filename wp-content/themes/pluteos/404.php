<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Templateheld
 */

 get_header();
 ?>
 <main id="content">

 	 <?php get_template_part( 'template-parts/content', 'none' ); ?>

 </main>

 <?php get_footer(); ?>
