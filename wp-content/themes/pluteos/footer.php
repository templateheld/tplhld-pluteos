<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Templateheld
 */

 $frontpage_id = get_option( 'page_on_front' );

?>

<footer id="footer">
	<div class="container-fluid">
		<div class="row">
			<nav id="footer-nav">
				<?php
					$menu = array(
						'container'       => '',
						'theme_location' => 'menu-2',
						'menu_id'        => 'footer-menu',
					);

					wp_nav_menu($menu);
				?>
        <?php
					$menu = array(
						'container'       => '',
						'theme_location' => 'menu-3',
						'menu_id'        => 'footer-menu',
					);

					wp_nav_menu($menu);
				?>
			</nav>
			<div class="logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php echo the_field('logo_p', $frontpage_id); ?>" alt="">
				</a>
				<div class="row">
					<div id="slogan">
						<h2><?php echo the_field('claim', $frontpage_id) ?></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

<script>
var privacy_de = '<?php echo get_permalink(166); ?>';
var privacy_en = '<?php echo get_permalink(132); ?>';

window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "rgba(112, 103, 118, 0.9)",
        "text": "#ffffff"
      },
      "button": {
        "background": "transparent",
        "text": "#ffffff",
        "border": "#ffffff"
      }
    },
    <?php if ( pll_current_language() == 'de' ): ?>
    "content": {
      "message": "Cookies helfen uns bei der Bereitstellung unserer Dienste. Durch die Nutzung unserer Dienste erklären Sie sich damit einverstanden, dass wir Cookies setzen. ",
      "dismiss": "Verstanden",
      "link": "Weitere Informationen zu Cookies und Datenschutz finden Sie hier.",
      "href": privacy_de
    }
    <?php else: ?>
    "content": {
      "message": "Cookies help us providing our website services. By using our services, you agree and accept that we are setting cookies.",
      "dismiss": "Accept",
      "link": "More information",
      "href": privacy_en
    }
    <?php endif; ?>
  })
});
</script>

</body>
</html>
