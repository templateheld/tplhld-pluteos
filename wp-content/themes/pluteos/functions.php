<?php
/**
 * Templateheld functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Templateheld
 */

if ( ! function_exists( 'templateheld_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function templateheld_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Templateheld, use a find and replace
	 * to change 'templateheld' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'templateheld', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'templateheld' ),
		'menu-2' => esc_html__( 'Footer', 'templateheld' ),
		'menu-3' => esc_html__( 'Footer2', 'templateheld' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'templateheld_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 250,
		'width'       => 250,
		'flex-width'  => true,
		'flex-height' => true,
	) );
}
endif;
add_action( 'after_setup_theme', 'templateheld_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function templateheld_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'templateheld_content_width', 640 );
}
add_action( 'after_setup_theme', 'templateheld_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function templateheld_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'templateheld' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'templateheld' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'templateheld_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function templateheld_scripts() {
	wp_enqueue_style( 'templateheld-style', get_template_directory_uri() . '/style.css' );
  // TODO: switch to minified CSS file
	#wp_enqueue_style( 'templateheld-style', get_template_directory_uri() . '/style.min.css' );

	wp_enqueue_script( 'templateheld-jquery', get_template_directory_uri() . '/js/min/jquery.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-lazy-load', get_template_directory_uri() . '/js/min/lazysizes.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-scrollspy', get_template_directory_uri() . '/js/min/jquery.appear.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-bootstrap-dependency', get_template_directory_uri() . '/js/min/popper.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-bootstrap', get_template_directory_uri() . '/js/min/bootstrap.min.js', array(), '', true );

	wp_enqueue_script( 'templateheld-navigation', get_template_directory_uri() . '/js/min/navigation.min.js', array(), '20151215', true );

	wp_enqueue_script( 'templateheld-skip-link-focus-fix', get_template_directory_uri() . '/js/min/skip-link-focus-fix.min.js', array(), '20151215', true );
	wp_enqueue_script( 'templateheld-cookie', get_template_directory_uri() . '/js/cookie.min.js', array(), '', true );
	wp_enqueue_script( 'templateheld-js', get_template_directory_uri() . '/js/min/main.min.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'templateheld_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// Polylang string translations
pll_register_string('Member Phone', 'PHONE', 'templateheld-strings');
pll_register_string('Member Email', 'EMAIL', 'templateheld-strings');
pll_register_string('Member Address', 'ADDRESS', 'templateheld-strings');
pll_register_string('Member Button small', 'VCF CARD', 'templateheld-strings');
pll_register_string('Member Vita', 'VITA', 'templateheld-strings');
pll_register_string('404 Title', 'Page not found', 'templateheld-strings');
pll_register_string('404 Text', 'Sorry, but we couldn\'t find the page you are looking for.', 'templateheld-strings');
pll_register_string('404 Button Text', 'Return to homepage', 'templateheld-strings');

// Add custom image sizes
add_image_size( 'laptop', 1440, 9999 ); // Soft

add_filter( 'image_size_names_choose', 'templateheld_custom_sizes' );

function templateheld_custom_sizes( $sizes ) {
  return array_merge( $sizes, array(
      'laptop' => __( 'Laptop' ),
  ) );
}

/**
* filter function to force wordpress to add our custom srcset values
* @param array  $sources {
*     One or more arrays of source data to include in the 'srcset'.
*
*     @type type array $width {
*          @type type string $url        The URL of an image source.
*          @type type string $descriptor The descriptor type used in the image candidate string,
*                                        either 'w' or 'x'.
*          @type type int    $value      The source width, if paired with a 'w' descriptor or a
*                                        pixel density value if paired with an 'x' descriptor.
*     }
* }
* @param array  $size_array    Array of width and height values in pixels (in that order).
* @param string $image_src     The 'src' of the image.
* @param array  $image_meta    The image meta data as returned by 'wp_get_attachment_metadata()'.
* @param int    $attachment_id Image attachment ID.

* @author: Aakash Dodiya
* @website: http://www.developersq.com
*/
add_filter( 'wp_calculate_image_srcset', 'dq_add_custom_image_srcset', 10, 5 );
function dq_add_custom_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ){

	// image base name
	$image_basename = wp_basename( $image_meta['file'] );
	// upload directory info array
	$upload_dir_info_arr = wp_get_upload_dir();
	// base url of upload directory
	$baseurl = $upload_dir_info_arr['baseurl'];

	// Uploads are (or have been) in year/month sub-directories.
	if ( $image_basename !== $image_meta['file'] ) {
		$dirname = dirname( $image_meta['file'] );

		if ( $dirname !== '.' ) {
			$image_baseurl = trailingslashit( $baseurl ) . $dirname;
		}
	}

	$image_baseurl = trailingslashit( $image_baseurl );
	// check whether our custom image size exists in image meta
	if( array_key_exists('laptop', $image_meta['sizes'] ) ){

		// add source value to create srcset
		$sources[ $image_meta['sizes']['laptop']['width'] ] = array(
				 'url'        => $image_baseurl .  $image_meta['sizes']['laptop']['file'],
				 'descriptor' => 'w',
				 'value'      => $image_meta['sizes']['laptop']['width'],
		);
	}

	//return sources with new srcset value
	return $sources;
}

// Allow custom file mimes in media upload
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
	$mimes['vcf'] = 'text/x-vcard';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
